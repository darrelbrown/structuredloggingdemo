﻿using System;
using Serilog;

namespace StructuredLoggingDemo
{
	class Program
	{
		private static ILogger _logger;

		static void Main(string[] args)
		{
			Log.Logger = new LoggerConfiguration()
				.Enrich.WithProperty("ApplicationName", "StructuredLoggingDemo")
				.WriteTo.ColoredConsole()
				.WriteTo.Seq("http://localhost:5341")
				.CreateLogger();

			_logger = Log.ForContext<Program>();

			DateTime currentTime = DateTime.Now;
			_logger.Information("Current time using standard string format pattern: {0:o}", currentTime);
			_logger.Information("Current time using Serilog format pattern: {StartTime:o}", currentTime);


			_logger.Warning("Shutting down now!");
		}
	}
}
