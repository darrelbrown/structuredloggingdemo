﻿namespace StructuredLoggingDemo
{
	public enum CharacterClass
	{
		Barbarian,
		Bard,
		Cleric,
		Druid,
		Fighter,
		Monk,
		Paladin,
		Ranger,
		Rogue,
		Sorcerer,
		Wizard
	}
}