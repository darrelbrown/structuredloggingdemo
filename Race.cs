namespace StructuredLoggingDemo
{
	public enum Race
	{
		Human, 
		Elf, 
		Dwarf,
		Halfling,
		HalfElf,
		HalfOrc,
		Gnome
	}
}