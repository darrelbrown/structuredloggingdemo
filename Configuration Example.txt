﻿var ravenLogs = new DocumentStore() {Url = "http://localhost:8080", DefaultDatabase = "LogDemo"}.Initialize();

Log.Logger = new LoggerConfiguration()
	.Enrich.WithMachineName()
	.Enrich.WithProcessId()
	.Enrich.With<UserNameEnricher>()
	.WriteTo.ColoredConsole()
	.WriteTo.Seq("http://localhost:5341")
	.WriteTo.RavenDB(ravenLogs)
	.CreateLogger();


Character roy = new Character("Roy Greenhilt", Race.Human, CharacterClass.Fighter);
Character haley = new Character("Haley Starshine", Race.Human, CharacterClass.Rogue);
Character belkar = new Character("Belkar Bitterleaf", Race.Human, CharacterClass.Ranger);
Character durkon = new Character("Durkon Thundershield", Race.Dwarf, CharacterClass.Cleric);
Character elan = new Character("Elan", Race.Human, CharacterClass.Bard);
Character vaarsuvius = new Character("Vaarsuvius", Race.Elf, CharacterClass.Wizard);