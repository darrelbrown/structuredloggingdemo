﻿using System;
using Serilog.Core;
using Serilog.Events;

namespace StructuredLoggingDemo.LogEnrichers
{
	public class UserNameEnricher : ILogEventEnricher
	{
		public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
		{
			logEvent.AddOrUpdateProperty(propertyFactory.CreateProperty("UserName", Environment.UserName));
		}
	}
}