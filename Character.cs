using Serilog;

namespace StructuredLoggingDemo
{
	public class Character
	{
		private static readonly ILogger _logger = Log.Logger.ForContext<Character>();

		public string CharacterName { get; private set; }
		public Race Race { get; private set; }
		public CharacterClass StartingClass { get; private set; }

		public Character(string characterName, Race race, CharacterClass startingClass)
		{
			CharacterName = characterName;
			Race = race;
			StartingClass = startingClass;

			_logger.Information("Created new character: {@character}", this);
		}
	}
}